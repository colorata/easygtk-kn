# EasyGtkKn

Easily manage project based on [gtk-kn](https://gitlab.com/gtk-kn/gtk-kn).

## Setup

1. Add `~/.local/bin` to `PATH`
2. Run `curl -o ~/.local/bin/easygtkkn https://gitlab.com/colorata/easygtk-kn/-/raw/main/easygtkkn && chmod +x ~/.local/bin/easygtkkn`

3. Install [Podman](https://podman.io/docs/installation)
   or [Docker](https://docs.docker.com/desktop/install/linux-install/) into your machine.

4. Initiate existing project with `./easygtkkn create REPO` where `REPO` is path to your project.
   E.g. `./easygtkkn create ~/Project/MyAwesomeProject/`
5. Add dependency to your project: `implementation("org.gtkkn:gtk4:0.0.1-SNAPSHOT")` or go
   to [hello gtk-kn world](https://gtk-kn.github.io/gtk-kn/user-guide/hello-world/)

## Running

1. Run `./easygtkkn run "./gradlew TASK"` where `TASK` is gradle task which you want to execute.
   E.g. `./easygtkkn run "./gradlew assemble"` to assemble project. **Please do not directly
   run GUI apps!**

## Erasing

1. If you do not need EasyGtkKn anymore, you can run `./easygtkkn remove_all` to remove all EasyGtkKn containers.