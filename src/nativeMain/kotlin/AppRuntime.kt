import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option

interface Runtime {
    val verbose: Boolean
    val config: Config
}

abstract class AppRuntime(name: String, help: String = "", envVar: String? = null) :
    CliktCommand(name = name, help = help, autoCompleteEnvvar = envVar), Runtime {
    override val config by requireObject<Config>()

    override val verbose by option(help = "Print every error").flag()

    val docker: String by lazy { config.docker }
}

fun defaultRuntime(verbose: Boolean): Runtime {
    return object : Runtime {
        override val config = Config("")
        override val verbose = verbose
    }
}