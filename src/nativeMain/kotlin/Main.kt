import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import platform.posix.exit
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

data class Config(val docker: String)

fun main(rawArgs: Array<String>) = EasyGtkKn().subcommands(Create(), Running(), RemoveAll(), Run()).main(rawArgs)

class EasyGtkKn : AppRuntime("") {
    override fun run() {
        currentContext.obj = Config(defaultRuntime(verbose).run {
            val docker = containerBinaryPath()
            checkContainerBinaryPath(docker)
            docker
        })
    }
}

class Create : AppRuntime("create", "Create repository with given path") {
    val repo by argument("repo_path", "Path to repository")
    override fun run() {
        val containerId = runInShellWithOutput("$docker run -d -t -v $repo:/repo:Z docker.io/colorata/easy-gtk-kn:latest")
        if (containerId != null) {
            val result = runInShell("$docker cp $containerId:/root/.m2/repository/org/ ${homePath()}/.m2/repository/")
            if (result == 0) printlnFormatted("Success!".bold())
            else printlnFormatted("Could not copy gtk-kn into ${homePath()}/.m2/")
        } else {
            error("Could not Init")
        }
    }
}

class Running : AppRuntime("running", "Get all running containers") {
    override fun run() {
        printlnFormatted("Containers:".bold())
        val runningIds = getContainerIds()
        val allIds = getContainerIds(includeNotRunning = true)
        allIds.forEachIndexed { index, id ->
            printlnFormatted("${index + 1}. ".bold() + id + if (id !in runningIds) " (stopped)".italic() else "")
        }
    }
}

class RemoveAll : AppRuntime("remove_all", "Remove all running containers") {
    override fun run() {
        val ids = getContainerIds().joinToString(" ")
        //runInShell("$docker stop $ids")
        runInShell("$docker rm -f $ids")
    }
}

class Run : AppRuntime("run", "Run command inside container") {
    val command by argument("command", "Command to execute inside terminal")
    override fun run() {
        val ids = getContainerIds()
        if (ids.isNotEmpty()) {
            val containerId = if (ids.size != 1) selectorOf(
                name = "Select container:",
                default = 1,
                allowedRange = 1..ids.size,
                options = ids.map { it + (" (" + getContainerNameFromId(it) + ")").italic() }.toTypedArray()
            ) else 1
            runInsideContainer(ids[(containerId ?: 1) - 1], command)
        }
    }
}

fun AppRuntime.getContainerIds(includeNotRunning: Boolean = false): List<String> {
    val out =
        runInShellWithOutput("$docker ps -${if (includeNotRunning) "a" else ""}qf \"ancestor=docker.io/colorata/easy-gtk-kn\"")
    if (out.isNullOrBlank()) return listOf()
    return out.split("\n")
}

fun AppRuntime.getContainerNameFromId(id: String): String? {
    return runInShellWithOutput("$docker inspect --format=\"{{.Name}}\" $id")
}

fun AppRuntime.runInsideContainer(containerId: String, command: String) {
    runInShell("$docker exec -it $containerId bash -c \"cd repo && $command\"")
}

fun AppRuntime.containerBinaryPath(): String? {
    return runInShellWithOutput("which podman") ?: runInShellWithOutput("which docker")
}

@OptIn(ExperimentalContracts::class)
fun checkContainerBinaryPath(path: String?) {
    contract {
        returns() implies (path != null)
    }
    if (path != null) {
        printlnFormatted("Found Docker at ${path.bold().italic()}")
    } else {
        println("Not found any container")
        exit(1)
    }
}