import kotlinx.cinterop.ExperimentalForeignApi
import kotlinx.cinterop.refTo
import kotlinx.cinterop.toKString
import platform.posix.*

fun AppRuntime.runInShell(command: String): Int {
    if (verbose) println(command)
    val suffix = if (verbose) "" else " 2>/dev/null"
    return system(command + suffix)
}

@OptIn(ExperimentalForeignApi::class)
fun envVar(key: String) = getenv(key)?.toKString()

fun homePath() = envVar("HOME") ?: error("No HOME path defined")

@OptIn(ExperimentalForeignApi::class)
fun AppRuntime.runInShellWithOutput(command: String): String? {
    val redirectStderr = !verbose
    if (verbose) printlnFormatted("Executing: ".bold() + command)
    val commandToExecute = if (redirectStderr) "$command 2>&1" else command
    val fp = popen(commandToExecute, "r") ?: error("Failed to run command: $command")

    val stdout = buildString {
        val buffer = ByteArray(4096)
        while (true) {
            val input = fgets(buffer.refTo(0), buffer.size, fp) ?: break
            append(input.toKString())
        }
    }

    val status = pclose(fp)
    if (status != 0) return null

    if (status != 0 && !verbose) {
        error("Command `$command` failed with status $status${if (redirectStderr) ": $stdout" else ""}")
    }

    if (stdout.isBlank()) return ""
    return if ('\n' == stdout.last()) stdout.dropLast(1) else stdout
}