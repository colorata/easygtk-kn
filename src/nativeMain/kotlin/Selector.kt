import com.github.ajalt.clikt.core.terminal
import com.github.ajalt.mordant.terminal.ConversionResult

fun AppRuntime.selectorOf(
    name: String,
    default: Int = 1,
    allowedRange: IntRange,
    vararg options: String
): Int? {
    options.forEachIndexed { index, id ->
        printlnFormatted("${index + 1}. ".bold() + id)
    }
    return terminal.prompt(name, default = default) { raw ->
        raw.toIntOrNull()
            ?.let { if (it in allowedRange) ConversionResult.Valid(it) else ConversionResult.Invalid("$it not in range 1..${allowedRange.last}") }
            ?: ConversionResult.Invalid("Invalid number")
    }
}