import platform.posix.system

fun printlnFormatted(line: String) {
    system("echo -e \'$line\'")
}

fun eraseLine() {
    system("tput cuu1")
}

private fun String.formattedWithKey(key: String) = "\\e[${key}m$this\\e[0m"

fun String.bold() = formattedWithKey("1")

fun String.italic() = formattedWithKey("3")

fun String.underline() = formattedWithKey("4")

fun String.strikethrough() = formattedWithKey("9")

fun String.withLink(link: String) = "\\e]8;;$link\\a$this\\e]8;;\\a"